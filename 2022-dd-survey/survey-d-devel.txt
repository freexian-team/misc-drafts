
To: debian-devel@lists.debianorg
Subject: What are the most important projects that Debian ought to work on?

Hello,

As part of an upcoming survey that we are preparing, we plan to
ask Debian developers to rank, by order of importance, the most popular
ideas of improvements for Debian.

However, there's no easy way to identify what are the most popular ideas of
improvements that Debian ought to make. We know of the "grow-your-ideas"
project on Salsa, but it's far from exhaustive:

https://salsa.debian.org/debian/grow-your-ideas

That's where you come into play: it would be nice if you could share
what are — according to you — the most important projects/improvements
that Debian ought to make. You can share your ideas here by replying
to this email, but it would be interesting to file them as new issues
in the "grow-your-ideas" project and then reply here pointing to your new issue:

https://salsa.debian.org/debian/grow-your-ideas/-/issues

Another useful thing to do would be to review the current ideas in that
project and up-vote those that you find interesting. You can do that by
opening the issue and clicking on the "thumbs-up" button. That way we
can easily identify the most popular ideas:

https://salsa.debian.org/debian/grow-your-ideas/-/issues?sort=popularity

(Note that you can watch the project or just some specific issues if you
want to participate in the exchanges happening in the various issues.)

For the record, the goal of this question in our upcoming survey is to
help us identify useful projects to fund as part of our project funding
initiative:

https://salsa.debian.org/freexian-team/project-funding

XXXXX on behalf of the Freexian team
